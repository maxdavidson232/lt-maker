# Frequently Asked Questions

## Audio

### My music is playing at a different pitch?

LT is configured at 44100 Hz. Most likely, your music was sampled at 48000 Hz. You can use
[Audacity](https://superuser.com/questions/420531/audacity-resampling) to resample your track
to the appropriate frequency.

### Does LT support `.tmx` map files?

No. LT uses `.png` files for the graphical component of its maps, and uses internal data for terrain data.